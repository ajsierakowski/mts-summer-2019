#!/usr/bin/env bash

#SBATCH --partition=debug
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mail-type=END
#SBATCH --mail-user=t-asierak1@jhu.edu

module load python/3.7

pwd
python example.py
echo 'Done!'
