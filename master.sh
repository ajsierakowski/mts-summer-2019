#!/bin/bash

# method 2: a master script writes and submits multiple jobs

for i in {1..10}
do
  mkdir out-$i
  cd out-$i
  echo '#!/bin/bash' > submit.sh
  echo '#SBATCH --partition=express' > submit.sh
  echo 'module load ...' > submit.sh
  echo "./my-genomics-app $i" > submit.sh
  sbatch submit.sh
done
