#!/bin/bash

if [ $1 == '-a' ]; then
    WHERE="artists.name='$2'"
elif [ $1 == '-t' ]; then
    WHERE="tracks.title='$2'"
fi

sqlite3 music "SELECT
  tracks.track_num,
  tracks.title,
  albums.title,
  artists.name,
  genres.name
  FROM 
  tracks
  INNER JOIN albums ON albums.rowid = tracks.album_id
  INNER JOIN artists on artists.rowid = albums.artist_id
  INNER JOIN genres ON genres.rowid = tracks.genre_id
  WHERE $WHERE
  ORDER BY tracks.title;"
