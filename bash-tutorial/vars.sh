#!/usr/bin/env bash

a=Hello
b=world

echo $a $b
echo

# full path guaranteed to hit same path every time
dir=/home-4/t-asierak1@jhu.edu/mts-summer-2019-mine/bash-tutorial
# relative path will depend on path from which script is run
#dir=.

ls $dir

# single quotes are literal strings
c='Hello world!'
echo $c

# double quotes substitute variables
d="HELLO $c"
echo $d

# save contents of command in variable
e=$(ls $dir | wc -l)
echo $e
