#!/usr/bin/env bash

# ask user for name
echo Enter the file name:

read file_name

echo $file_name

date=$(date +%F)
touch $file_name-$date

# other uses of read
read -p 'File name: ' file_name_2
echo $file_name_2

# hide the input
read -sp 'Password: ' passvar
echo Thanks for your password $passvar
