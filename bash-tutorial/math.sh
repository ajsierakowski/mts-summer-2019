#!/usr/bin/env bash

let a=30/2

echo $a

# same thing different syntax
b=$((1+3))

echo $b

function print_hello {
  echo HELLO!
}

print_hello
