#!/usr/bin/env bash

# read command line arguments

echo $0
echo $1
echo $2
echo $3

echo The number of args is: $#

echo All of the args together is: $@

echo $USER
#echo $PATH

echo $RANDOM

date=$(date +%F)
#*** $# does not count the first arg
if [ "$#" -ne 1 ]; then
  echo 'usage: ./args.sh <filename>'
else
  touch $1-$date
fi
