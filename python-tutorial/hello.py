#!/usr/bin/env python3

# single and double quotes are  EXACTLY the same
print("Hello world")
print('Hello world')

a = 1
b = 2
c = a + b

print(c)
