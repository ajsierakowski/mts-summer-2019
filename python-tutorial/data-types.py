#!/usr/bin/env python3

# STRINGS #

H = 'Hopkins'
print(H)
print(len(H))
print(H[0], H[6], H[-3])
print(H[0:3], H[3:], H[:5])
print(H + ' University')
print(H * 4)
print(H.find('pki'))
print(H.find('pi'))
print(H.replace('op', 'ad'))
#H = H.replace('op', 'ad')
print(H)

# LISTS #

L = [6, 8, 7, 5, 3, 0, 9]
print(L)
print(L[3:])
L[3] = 0
print(L)
L = ['JHU', 1876, None]
print(L)
print(L[2] == True)
L[2] = True
print(L)
L.append('Baltimore')
print(L)
print(L.pop(2))
print(L)
L[1] = 'Homewood'
L.sort()
print(L)

# DICTIONARIES #

D = {
    'school': 'Hopkins',
    'nstudents': 12345
}
print(D)
print(D['school'])
D['city'] = 'Baltimore'
print(D)
D['buildings'] = ['Bloomberg', 'Malone', 'Latrobe', 'PCTB']
print(D)
print(D['buildings'][-1])

# FILES #

f = open('hello.py', 'r')
text = f.read()
print(text)

f.close()
f = open('hello.py', 'r')

for l in f:
    print(l)
    print('looping')

print('done loop')
