#!/usr/bin/env python3

# method 1:
# imports entirety of my_functions
#import my_module.my_functions

# method 2:
# imports only the functions listed
#from my_module.my_functions import check_length
#from my_module.my_functions import get_input

# method 3:
# imports only the functions listed with aliases
from my_module.my_functions import check_length as cl
from my_module.my_functions import get_input as gi

# get user input
# method 1:
#user_in = my_functions.get_input()
# method 2:
#user_in = get_input()
# method 2:
user_in = gi()

# repeat until a 'q' appears
while user_in.find('q') != 0:
    # method 1:
    #my_functions.check_length(user_in)
    # method 2:
    #check_length(user_in)
    # method 3:
    cl(user_in)

    # print each character
    for i in range(len(user_in)):
        print('    user_in[' + str(i) + '] = ' + user_in[i])

    # print each character with built-in iterator
    i = 0
    for char in user_in:
        print('    user_in[' + str(i) + '] = ' + char)
        i += 1

    # get user input again
    # method 1:
    #user_in = my_functions.get_input()
    # method 2:
    #user_in = get_input()
    # method 3:
    user_in = gi()

# notify complete
print('Done!')
