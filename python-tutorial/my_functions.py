# define the input query
def get_input():
    return input('Type something: ')

def check_length(string):
    # determine whether this is a string
    if len(string) < 1:
        print('  Empty')
    elif len(string) < 2:
        print('  Character')
    else:
        print('  String')
