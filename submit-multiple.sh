#!/usr/bin/env bash

#SBATCH --partition=debug
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mail-type=END
#SBATCH --mail-user=t-asierak1@jhu.edu

module load python/3.7

# method 1: job arrays

out_dir=out_$SLURM_ARRAY_TASK_ID
#cp -r template $out_dir
cd $out_dir
./my-genomics-application input.txt
