#include <stdlib.h>
#include <stdio.h>

#include "readwrite.h"

#include <factorial.h>

int main(int argc, char *argv[]) {
  printf("Hello world!\n");

  printf("There are %d command line arguments\n", argc);
  printf("They are:\n");

  for(int i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // create a static array
  int A[10];

  // fill and print A
  for(int i = 0; i < 10; i++) {
    //printf("A[%d] = %d\n", i, A[i]);
    A[i] = i;
    printf("A[%d] = %d\n", i, A[i]);
  }

  // read a file
  float a = 0;
  float b = 0;
  float c = 0;

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  read("input.txt", &a, &b, &c);

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  printf("5! = %d\n", factorial(5));

  return EXIT_SUCCESS;
}
